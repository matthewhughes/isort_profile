from typing import Any, Dict

from isort.profiles import black

PROFILE: Dict[str, Any] = {
    **black,
    "force_single_line": True,
}
