``isort_profile``
===============

An example of a shared ``isort`` profile. Just as a demonstration, this profile
is based of the builtin ``black`` profile that ships with ``isort``.

A profile is defined at ``isort_profile.PROFILE`` which is then specified in
``setup.cfg`` under ``options.entry_points``.

There's a quick demonstration of this working in the tests.
