from textwrap import dedent

import isort


def test_formats_as_expected():
    sample_imports = dedent(
        """\
        from foo import bar, baz
    """
    )

    expected_output = dedent(
        """\
        from foo import bar
        from foo import baz
    """
    )

    assert isort.code(sample_imports, profile="my_profile") == expected_output
